package com.example.com;

import android.content.Context;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

public class Repository {

    public interface OnResponseListener<T> {

        public void onSuccess(T response);

        public void onError(Exception error);
    }

    private RequestQueue requestQueue;
    private static Repository instance;

    private Repository(Context context) {

        this.requestQueue = Volley.newRequestQueue(context);
    }

    public static Repository getInstance(Context context) {

        if (instance == null) instance = new Repository(context);
        return instance;
    }

    public void getUsers(final OnResponseListener<User[]> onResponseListener) {

        GsonRequest<User[]> request =
                new GsonRequest<User[]>(
                        Request.Method.GET,
                        "https://api.github.com/users",
                        User[].class,
                        new Response.Listener<User[]>() {

                            @Override
                            public void onResponse(User[] response) {

                                onResponseListener.onSuccess(response);
                            }
                        },
                        new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {

                                onResponseListener.onError(error);
                            }
                        });
        requestQueue.add(request);
    }
}
