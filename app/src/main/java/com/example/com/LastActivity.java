package com.example.com;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class LastActivity extends AppCompatActivity {

    private TextView textview_1_1;
    private ImageView imageview_1_0;
    private MockModel mockmodel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.lastactivity);

        if (getIntent().getExtras() != null) {
            String data = getIntent().getStringExtra("data");
            mockmodel = MockModel.fromJson(data);
        }

        textview_1_1 = (TextView) findViewById(R.id.textview_1_1);
        imageview_1_0 = (ImageView) findViewById(R.id.imageview_1_0);

        imageview_1_0.setImageResource(mockmodel.getImage());
        textview_1_1.setText(mockmodel.getLabel());
    }
}
